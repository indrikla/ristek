from django.apps import AppConfig


class RistekappConfig(AppConfig):
    name = 'ristekapp'
